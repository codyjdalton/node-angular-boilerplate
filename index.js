'use strict';

// include dependencies
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

//apply body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// include custom routes
var routes = require('./routes');

// set port
app.set('port', (process.env.PORT || 3000));

//set public directory
app.use(express.static(__dirname + '/app'));

// set views directory
app.set('views', __dirname + '/app');
app.set('view engine', 'jade');

// send to index
app.get('/', routes.index);

// api service
app.use('/api/:resource', routes.api);
app.use('/api', routes.api);

// render jade templates
app.get('/tpl/:type/:resource/:template', routes.template);

//intialize application
app.listen(app.get('port'), function () {
    console.log('Application launched on port', app.get('port'));
});

'use strict';

// create the module and name it app
// also include Angular UI Router for all our routing needs
var app = angular.module('app', ['ui.router', 'ngStorage']);

// create the controller and inject Angular's $scope
app.controller('appCtrl', function ($scope, $http, $localStorage) {

    //check if configuration data saved to local (client side) storage
    if($localStorage.configuration){

        // sync scope with local storage
        $scope.configuration = $localStorage.configuration;

        return;
    }

    /* config data is not set */

    //get configuration data from API
    $http.get("/api/configurations")
        .then(function (response) {
            //test config response
            if (response.data && response.data.results && response.data.results[0]) {
                
                // set as configuration scope
                $scope.configuration = response.data.results[0];

                // set configuration to local storage
                $localStorage.configuration = response.data.results[0];

                // all good
                return;
            }
        });
});

//set up a config router
app.config(function ($stateProvider, $urlRouterProvider) {

    //default to home state
    $urlRouterProvider.otherwise('/home');
    $stateProvider
    //load views for individual states
        .state('home', {
            url: '/home',
            templateUrl: 'tpl/components/home/homeView'
        })
        .state('services', {
            url: '/services',
            templateUrl: 'tpl/components/services/servicesView'
        })
        .state('contact', {
            url: '/contact',
            templateUrl: 'tpl/components/contact/contactView'
        });
});

'use strict';

// include dependencies
var expect = require('chai').expect;
var request = require('superagent');

// describe a test situation
describe('API Service', function() {

    // set baseURL
    var baseUrl = 'http://localhost:3000';

    // api entry point
    describe('when requested at /api', function() {

        it('should return JSON response with status 200', function(done) {
            request.get(baseUrl + '/api').end(function assert(err, res) {

                //oh noes!
                expect(err).to.not.be.ok;

                //expect 200 ok
                expect(res).to.have.property('status', 200);

                done();
            });
        });
    });
});


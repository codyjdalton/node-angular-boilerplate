'use strict';

/**
 * Custom Controller
 *
 * Duplicate/replace this with your own code.
 *
 */
module.exports = function (req, res) {

    // post
    if (req.method === 'POST') {
        //create a new contact
        res.json({
            status: 201,
            message: "created"
        });

        return;
    }

    // get
    if (req.method === 'GET') {
        //create a new contact
        res.json({
            status: 200,
            message: "retrieved"
        });

        return;
    }

    // default response
    res.json({
        status: 400,
        message: req.method + " method not yet supported."
    });

    return;
};

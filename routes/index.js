'use strict';

/* Routes */
var api = require('../routes/api');

/**
 * Application entry point
 */
exports.index = function (req, res) {

    res.render('index'); //render index file
};

/**
 * Serve angular templates
 */
exports.template = function (req, res) {
    res.render(req.params.type + '/' + req.params.resource + '/' + req.params.template);
};

/**
 * Starter API
 */
exports.api = api;

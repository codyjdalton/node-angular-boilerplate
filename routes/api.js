'use strict';
/**
 * Programming Interface
 * A very basic API to get you started
 */

var config = require('../package.json');
var contact = require('../routes/contact');

/**
 * Returns the configuration data for the application
 * @return JSON object
 */
var configurations = function (req, res) {

    //add only the items from package.json that are needed
    var configItem = {
        version: config.version,
        title: config.title
    };

    //send response to client
    res.json({
        status: 200,
        results: [configItem]
    });
};

/**
 * Object of defined resources
 * @type object
 */
var resources = {
    configurations: configurations,
    contact: contact //example of a custom contact module
};

/**
 * API Entry point
 */
module.exports = function (req, res) {

    //check if resource defined
    var resource = req.params.resource;

    if (!resource || !resources[resource]) {
        //show default response
        res.json({
            status: 200,
            message: 'Welcome to the API. Try adding a valid /resource to get started.'
        }).end();
        return;
    }
    //resource function exists, send to resource function
    resources[resource](req, res);
};

# Node Angular Boilerplate

Unassuming component-based boilerplate for your node-angular application. The idea here is that you can grab it and go with any project idea. Works great for prototypes and quick demos.

[View the demo](http://node-angular-boiler.herokuapp.com/)

## Installation

Clone the repository or download the zip file:
	
	git clone https://github.com/codyjdalton/node-angular-boilerplate.git
	cd node-angular-boilerplate
	npm install
	node index.js

To automatically recompile LESS modifications:

	sudo npm install -g grunt-cli
	grunt watch
	
## Stack
	
The boilerplate uses a number of open source projects to work properly:

* [Angular JS](https://github.com/angular/angular) - HTML enhanced web apps!
* [Node JS](https://github.com/nodejs/node) - event-driven, non-blocking I/O
* [npm](https://github.com/nodejs/node) - JS package manager
* [Express JS](https://github.com/expressjs/express) - web application server framework
* [Jade](https://github.com/npm/npm) - clean, whitespace sensitive syntax for writing html
* [Grunt](https://github.com/gruntjs/grunt) - for automated tasking
* [Mocha JS](https://github.com/mochajs/mocha) - javascript test framework for node.js & the browser
* [Bootstrap 3](https://github.com/twbs/bootstrap) - twitter's master plan for world domination
* [jQuery](https://github.com/jquery/jquery) - for now...

## Run Tests

To run unit tests:

	npm test

### License

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.